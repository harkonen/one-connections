restart
needsPackage "GraphicalModels"

G = mixedGraph(digraph{{0,1},{1,2},{2,3},{0,2}}, bigraph{{1,3}})
--G = mixedGraph(digraph{{0,1},{0,2}}) 
R = gaussianRing G

trekIdeal(R,G)
gaussianVanishingIdeal R

S = support covarianceMatrix R
O = support bidirectedEdgesMatrix R
L = support directedEdgesMatrix R


deg2s = basis(2,R,Variables=>S);
deg2o = basis(2,R,Variables=>O);

imss = unique flatten entries gaussianParametrization R;
ss = unique flatten entries covarianceMatrix R;

phi = map(R,R,apply(ss,imss,(i,j) -> i => j));
c = flatten entries phi deg2s;
r = flatten entries deg2o;

M = table(r,c, (row,col) -> first entries monomials(col//row));

-- Final algorithm
listSs = first entries deg2s;
while true do (
	tM = transpose M;
	cols = #tM;
	singles = 
		M / 
		flatten / 
		tally / 
		pairs / 
		(l -> select(l, i->i#1 == 1) / (j->j#0));
	colsToRemove = apply(singles, M, (i,j) -> positions(j, k -> any(i, l -> member(l, k))));
	for i in rsort unique flatten colsToRemove do (
		tM = drop(tM, {i,i});
		listSs = drop(listSs, {i,i});
	);
	oldCols = cols;
	cols = #tM;
	M = transpose tM;
	if oldCols == cols then break;
)







---- Cyclic example
restart
needsPackage "GraphicalModels"

G = mixedGraph(digraph{{0,1},{0,2},{1,2},{2,3},{3,1}})
R = gaussianRing G

trekIdeal(R,G)
gaussianVanishingIdeal R

Omega = bidirectedEdgesMatrix R
Lambda = directedEdgesMatrix R
I = map(R^4,R^4,1)
M = (I - Lambda)
minor = (M,i,j) -> (
	submatrix(M, toList(set(0..3) - {i}), toList(set(0..3) - {j}))
)
cfac = matrix table(toList(0..3),toList(0..3),(i,j) -> (-1)^(i+j) * det(minor(M,i,j)))
adj = transpose cfac

gp = transpose adj * Omega * adj

S = support covarianceMatrix R
O = support bidirectedEdgesMatrix R
L = support directedEdgesMatrix R


deg6s = basis(6,R,Variables=>S);
deg6o = basis(6,R,Variables=>O);

imss = unique flatten entries gp;
ss = unique flatten entries covarianceMatrix R;

phi = map(R,R,apply(ss,imss,(i,j) -> i => j));
-- I = ideal(apply(ss,imss, (i,j)->i-j))
c = flatten entries phi deg6s;
r = flatten entries deg6o;
listSs = first entries deg6s;

-- First pass, kill lead monomial
M = matrix{c} // matrix{r};
M = (entries M / (i-> i/(j->if j == 0 then -1 else leadTerm j)));
while true do (
	tM = transpose M;
	cols = #tM;
	rowMax = M / max;
	tallyM = M / tally;
	rowsWithMax = positions(apply(rowMax, tallyM, (m,t) -> t#m == 1), i->i);
	colsToRemove = rowsWithMax / (i -> position(M#i, j -> j==rowMax#i));
	for i in rsort unique flatten colsToRemove do (
		tM = drop(tM, {i,i});
		listSs = drop(listSs, {i,i});
		c = drop(c, {i,i});
	);
	oldCols = cols;
	cols = #tM;
	print cols;
	M = transpose tM;
	if oldCols == cols then break;
)


-- detect all -1 rows
rowToRemove = positions(M, i->all(i, j-> j == -1));
print(#rowToRemove)
for i in rsort unique flatten rowToRemove do (
	M = drop(M, {i,i});
	r = drop(r, {i,i});
);


-- Final pass
elapsedTime M = entries (matrix{c} // matrix{r}) / (i -> i / (j -> first entries monomials j));
while true do (
	tM = transpose M;
	cols = #tM;
	singles = 
		M / 
		flatten / 
		tally / 
		pairs / 
		(l -> select(l, i->i#1 == 1) / (j->j#0));
	colsToRemove = apply(singles, M, (i,j) -> positions(j, k -> any(i, l -> member(l, k))));
	for i in rsort unique flatten colsToRemove do (
		tM = drop(tM, {i,i});
		listSs = drop(listSs, {i,i});
		c = drop(c, {i,i});
	);
	oldCols = cols;
	cols = #tM;
	print cols;
	M = transpose tM;
	if oldCols == cols then break;
)

M = matrix{c} // matrix{r};
N = selectInSubring(1,syz(M))
(matrix{listSs} * N)_(0,0)