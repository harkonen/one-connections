# one-connections

Supplementary material for the paper "One-connection rule for structural equation models".
The `.wl` files are sets of graphs (in Wolfram Language), `.nb` are Mathematica notebooks, and `.csv` files are computational results.  

## File list
- `cycleChains.wl` Cycle chains as described in section 4.4.1. Ranges from $`d = 1,\dotsc, 10`$ and $`2\ell = 2,\dotsc,20`$. The file contains a list, where each entry is the list $`(d, 2\ell, G)`$, where $`G`$ is a mixed graph.
- `cycleChains.csv` Timing results for runs using graphs from `cycleChains.wl`. A $`100 \times 4`$ table. Columns `ncyc`, `lcyc` correspond to $`n, 2\ell`$ respectively. Columns `nai`, `oc` are times (in seconds) taken by the "naive" and "one-connection" algorithms respectively. A value of `-1` indicates a timeout (>10 min).
- `randCon.wl` The set of 363 random (connected) sparse Erdos-Renyi graphs described in Section 4.4.2. The file contains a list of length 363. Each entry is the list $`(n, p_D, p_B, c, G)`$, where $`G`$ is a mixed graph.
- `randomConnected.csv` Timing results for runs using graphs in `randCon.wl`. A $`363 \times 6$` table. Columns `vtx`, `pd`, `pb`, `cyc` correspond to $`n, p_D, p_B, c`$ respectively. Columns `nai`, `oc` are times (in seconds) taken by the "naive" and "one-connection" algorithms respectively. A value of `-1` indicates a timeout (>10 min).
- `1-connection.nb` A Mathematica notebook describing the one-connection algorithm. Contains also implementations of the "one-connection" algorithm and "naive" algorithm as Mathematica functions.
- `sec5.m2` Supplementary Macaulay2 code for Section 5. Implements Algorithms 4 and 5, with some examples. In particular, we present the computation of Example 5.2.